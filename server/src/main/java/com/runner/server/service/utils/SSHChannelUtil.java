package com.runner.server.service.utils;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author 星空梦语
 * @desc
 * @date 2021/3/19 下午3:17
 */

@Component
public class SSHChannelUtil {
    private static Session sessionRedis,seeesionZk; //zk代理
    private static Channel channelRedis,channelZk;  //redis代理

    private static String sessionName;
    private static String sessionHost;
    private static String sessionPassword;
    private static String forwardRedis;
    private static String forwardZk;


    @Value("${session_host}")
    public  void setSessionHost(final String sessionHost){
        this.sessionHost=sessionHost;
    }

    @Value("${session_name}")
    public  void setSessionName(final String sessionName){
        this.sessionName=sessionName;
    }

    @Value("${session_password}")
    public  void setSessionPassword(final String sessionPassword){
        this.sessionPassword=sessionPassword;
    }

    @Value("${forward_redis}")
    public  void setSessionForwardRedis(final String forwardRedis){
        this.forwardRedis=forwardRedis;
    }

    @Value("${forward_zk}")
    public  void setSessionForwardZk(final String forwardZk){
        this.forwardZk=forwardZk;
    }

    public static void connectSSHForward(boolean openSSH) {
        if(!openSSH){
            return;  //不走代理
        }
        if(seeesionZk==null){
            try {
                JSch jsch = new JSch();
                //登陆跳板机
                seeesionZk = jsch.getSession(sessionName, sessionHost, 22);
                seeesionZk.setPassword(sessionPassword);
                seeesionZk.setConfig("StrictHostKeyChecking", "no");
                seeesionZk.connect();
                //建立通道
                channelZk = seeesionZk.openChannel("session");
                channelZk.connect();
                //通过ssh连接到远程机器
                seeesionZk.setPortForwardingL("127.0.0.1",2181, forwardZk, 2181);
                LogUtil.info("ssh代理连接zk成功.......->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if(sessionRedis==null){
            try {
                JSch jsch = new JSch();
                //登陆跳板机
                sessionRedis = jsch.getSession(sessionName, sessionHost, 22);
                sessionRedis.setPassword(sessionPassword);
                sessionRedis.setConfig("StrictHostKeyChecking", "no");
                sessionRedis.connect();
                //建立通道
                channelRedis = sessionRedis.openChannel("session");
                channelRedis.connect();
                //通过ssh连接到远程机器
                sessionRedis.setPortForwardingL("127.0.0.1",6379, forwardRedis, 6379);
                LogUtil.info("redis代理连接成功.......->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
