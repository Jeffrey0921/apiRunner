package com.runner.server.service.utils;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.ReferenceConfig;
import com.alibaba.dubbo.config.RegistryConfig;
import com.alibaba.dubbo.config.utils.ReferenceConfigCache;
import com.alibaba.dubbo.rpc.service.GenericService;
import com.alibaba.fastjson.JSON;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;
import sun.rmi.runtime.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Component
public class ZookeeperUtil {
    public   static ZooKeeper zookeeper;

    // 注册中心信息缓存
    public static Map<String, RegistryConfig> registryConfigCache = new ConcurrentHashMap<>();

    // 各个业务方的ReferenceConfig缓存
    public static Map<String, ReferenceConfig> referenceCache = new ConcurrentHashMap<>();



    public static RegistryConfig getRegistryConfig(String zkAddress){
        RegistryConfig registryConfig=registryConfigCache.get(zkAddress);
        if(registryConfig!=null){
            return  registryConfig;
        }else{
            registryConfig = new RegistryConfig();
            registryConfig.setAddress("zookeeper://"+zkAddress);
            registryConfig.setProtocol("zookeeper");
            registryConfigCache.put(zkAddress,registryConfig);
            return registryConfig;
        }
    }

    /**
     * @description  获取实例
     * @author 星空梦语
     * @date 2021/7/23 下午9:01
     */
    public static ReferenceConfig<GenericService> getReference(String interfaceName, String zkAddress, String version, String application, String group){
        String key=zkAddress+"_"+interfaceName;
        ReferenceConfig<GenericService>  referenceConfig=referenceCache.get(key);
        if(referenceConfig!=null){
            return  referenceConfig;
        }else{
            //存在一个问题，，无法连接到服务提供者，首次建立连接失败后，后续从缓存中获取GenericService的都是null
            ApplicationConfig app = new ApplicationConfig();
            referenceConfig = new ReferenceConfig<GenericService>();
            app.setName("apiRunner");
            app.setRegistry(getRegistryConfig(zkAddress));
            referenceConfig.setApplication(app);
            referenceConfig.setVersion(version);
            referenceConfig.setInterface(interfaceName);
            referenceConfig.setTimeout(3000);
            if(group!=null && !group.equals("default") ){
                referenceConfig.setGroup(group);
            }
            referenceConfig.setGeneric(true);
            referenceConfig.setRegistry(getRegistryConfig(zkAddress));
            referenceCache.put(key,referenceConfig);
            return referenceConfig;
        }

    }


    /**
     * @description zk连接
     * @author 星空梦语
     * @date 2021/3/17 下午6:12
     */
    public static void connectZookeeper(String host) throws Exception{
        zookeeper = new ZooKeeper(host, 3000, new Watcher() {
            @Override
            public void process(WatchedEvent watchedEvent) { }
        });

    }


    /**
     * @description 获取子节点
     * @author 星空梦语
     * @date 2021/3/17 下午6:12
     */
    public static List<String> getChildren(String path){
        try{
            return zookeeper.getChildren(path, false);
        }catch (Exception e){
            try {
                zookeeper.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return new ArrayList<>();
    }


    /**
     * @description 获取数据
     * @author 星空梦语
     * @date 2021/3/17 下午6:12
     */
    public static String getData(String path) throws Exception{
        byte[] data = zookeeper.getData(path, false, null);
        if (data == null) {
            return "";
        }
        return new String(data);
    }


}
